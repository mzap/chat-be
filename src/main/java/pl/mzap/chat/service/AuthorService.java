package pl.mzap.chat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import pl.mzap.chat.model.Author;
import pl.mzap.chat.repository.AuthorRepository;
import pl.mzap.chat.repository.entity.AuthorEntity;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthorService {

    private final AuthorRepository authorRepository;

    public Collection<Author> getAuthors() {
        return authorRepository.findAll().stream()
                .map(AuthorEntity::toDTO)
                .collect(Collectors.toList());
    }

    public long addAuthor(Author author) {
        Optional<Long> lastAuthorId = getAuthors().stream()
                .map(Author::getId)
                .max(Long::compare);
        author.setId(1);
        lastAuthorId.ifPresent(id -> author.setId(id + 1));
        return authorRepository.save(author.toEntity()).getId();
    }

    public byte[] getIcon(long iconId) throws IOException {
        File icon = ResourceUtils.getFile("classpath:avatars/" + iconId + ".jpg");
        return Files.readAllBytes(icon.toPath());
    }

    public byte[] getAuthorIcon(long authorId) throws IOException {
        AuthorEntity author = authorRepository.findById(authorId);
        File icon = ResourceUtils.getFile("classpath:avatars/" + author.getIconId() + ".jpg");
        return Files.readAllBytes(icon.toPath());
    }


    public Author getAuthorById(long authorId) {
        return authorRepository.findById(authorId).toDTO();
    }

    public void deleteAuthor(long authorId) {
        authorRepository.deleteById(authorId);
    }
}
