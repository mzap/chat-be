package pl.mzap.chat.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.mzap.chat.model.Author;
import pl.mzap.chat.model.Message;
import pl.mzap.chat.repository.AuthorRepository;
import pl.mzap.chat.repository.MessageRepository;
import pl.mzap.chat.repository.entity.AuthorEntity;
import pl.mzap.chat.repository.entity.MessageEntity;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MessageService {

    private final MessageRepository messageRepository;
    private final AuthorRepository authorRepository;

    public Collection<Message> getMessages() {
        List<Message> messages = messageRepository.findAll().stream()
                .map(this::completeMessageWithAuthor)
                .collect(Collectors.toList());
        Collections.reverse(messages);
        return messages;
    }

    private Message completeMessageWithAuthor(MessageEntity message){
        long authorId = message.getAuthorId();
        Author author = authorRepository.findById(authorId).toDTO();
        return message.toDTO(author);
    }

    public Message addMessage(Message message) {
        Optional<Long> lastMessageId = getMessages().stream()
                .map(Message::getId)
                .max(Long::compare);
        MessageEntity entity = messageRepository.save(message.toEntity(lastMessageId.map(id -> id + 1).orElse(1L), LocalDateTime.now()));
        return entity.toDTO(message.getAuthor());
    }

    public void deleteMessage(long messageId) {
        messageRepository.deleteById(messageId);
    }

    public void deleteMessages() {
        messageRepository.deleteAll();
    }
}
