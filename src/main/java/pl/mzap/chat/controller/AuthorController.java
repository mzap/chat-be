package pl.mzap.chat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import pl.mzap.chat.model.Author;
import pl.mzap.chat.service.AuthorService;

import java.io.IOException;
import java.util.Collection;

@RestController
@RequiredArgsConstructor
@RequestMapping("authors")
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping
    public Collection<Author> getAuthors() {
        return authorService.getAuthors();
    }

    @GetMapping("{authorId}")
    public Author getAuthorById(@PathVariable long authorId) {
        return authorService.getAuthorById(authorId);
    }

    @PostMapping
    public long addAuthor(@RequestBody Author author) {
        return authorService.addAuthor(author);
    }

    @DeleteMapping("{authorId}")
    public void deleteAuthor(@PathVariable long authorId) {
        authorService.deleteAuthor(authorId);
    }

    @GetMapping(value = "icon/{iconId}", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getIcon(@PathVariable long iconId) throws IOException {
        return authorService.getIcon(iconId);
    }

    @GetMapping(value = "{authorId}/icon", produces = MediaType.IMAGE_JPEG_VALUE)
    public byte[] getAuthorIcon(@PathVariable long authorId) throws IOException {
        return authorService.getAuthorIcon(authorId);
    }

}
