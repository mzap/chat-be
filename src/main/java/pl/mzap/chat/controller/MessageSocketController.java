package pl.mzap.chat.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import pl.mzap.chat.model.Message;
import pl.mzap.chat.service.MessageService;

@Controller
@Slf4j
@RequiredArgsConstructor
public class MessageSocketController {

    private final MessageService messageService;

    @MessageMapping("/messages")
    @SendTo("/chat/messages")
    public Message getMessage(Message message) {
        log.info("MSG: {}", message);
        return messageService.addMessage(message);
    }

}
