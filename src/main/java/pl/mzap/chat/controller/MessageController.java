package pl.mzap.chat.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import pl.mzap.chat.model.Message;
import pl.mzap.chat.service.MessageService;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("messages")
@RequiredArgsConstructor
public class MessageController {

    private final MessageService messageService;

    @GetMapping
    public Collection<Message> getMessages() {
        return messageService.getMessages();
    }

    @PostMapping
    public Message addMessage(@Valid @RequestBody Message message) {
        return messageService.addMessage(message);
    }

    @DeleteMapping("{messageId}")
    public void deleteMessage(@PathVariable long messageId) {
        messageService.deleteMessage(messageId);
    }

    @DeleteMapping
    public void deleteMessages() {
        messageService.deleteMessages();
    }

}
