package pl.mzap.chat.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mzap.chat.repository.entity.MessageEntity;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    private long id;
    private String content;
    private Author author;
    private String createDate;

    public MessageEntity toEntity(Long messageId, LocalDateTime createdDate) {
        return MessageEntity.builder()
                .id(messageId)
                .content(content)
                .createDate(createdDate)
                .authorId(author.getId())
                .build();
    }

}
