package pl.mzap.chat.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.mzap.chat.repository.entity.AuthorEntity;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Author implements Serializable {

    private long id;
    private String nickname;
    private int iconId;

    public AuthorEntity toEntity() {
        return AuthorEntity.builder()
                .id(id)
                .iconId(iconId)
                .nickname(nickname)
                .build();
    }

}
