package pl.mzap.chat.repository.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import pl.mzap.chat.model.Author;

import java.io.Serializable;

@RedisHash("Author")
@Data
@Builder
public class AuthorEntity implements Serializable {

    @Id
    private long id;
    private String nickname;
    private int iconId;

    public Author toDTO() {
        return Author.builder()
                .id(id)
                .nickname(nickname)
                .iconId(iconId)
                .build();
    }

}
