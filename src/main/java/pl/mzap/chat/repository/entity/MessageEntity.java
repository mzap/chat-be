package pl.mzap.chat.repository.entity;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import pl.mzap.chat.model.Author;
import pl.mzap.chat.model.Message;

import java.io.Serializable;
import java.time.LocalDateTime;

@RedisHash("Message")
@Data
@Builder
public class MessageEntity implements Serializable {

    @Id
    private long id;
    private String content;
    private LocalDateTime createDate;
    private boolean isRead;
    private long authorId;

    public Message toDTO(Author author) {
        return Message.builder()
                .id(id)
                .content(content)
                .author(author)
                .createDate(createDate.toString())
                .build();
    }

}
