package pl.mzap.chat.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mzap.chat.repository.entity.MessageEntity;

import java.util.Collection;

@Repository
public interface MessageRepository extends CrudRepository<MessageEntity, Long> {

    Collection<MessageEntity> findAll();

}
