package pl.mzap.chat.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.mzap.chat.repository.entity.AuthorEntity;

import java.util.Collection;

@Repository
public interface AuthorRepository extends CrudRepository<AuthorEntity, Long> {

    Collection<AuthorEntity> findAll();

    AuthorEntity findById(long id);

}
