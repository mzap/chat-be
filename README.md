# Chat Backend
App as backend for chat. Using websocket with STOMP to pass messaging between clients.
#### Build
`docker build -t chat .`
#### Run
`docker run -d -p 8081:8081 --name chat-be chat`
#### Log
`docker logs chat-be -f`