FROM maven:3-jdk-11 AS builder

ADD src /chat/src
ADD pom.xml /chat/pom.xml
WORKDIR /chat

RUN ls -l
RUN mvn clean install

FROM openjdk:11-jre-slim AS runner
RUN ls -l
EXPOSE 8081
VOLUME /tmp

COPY --from=builder "/chat/target/chat-*.jar" chat.jar
CMD [ "sh", "-c", "java -jar /chat.jar" ]